# time-pattern-mapping: Tool for Time Pattern Normalization and Mapping

author: [Cheng-Jen Lee](http://about.me/Sollee)

# Introduction

This tool is designed to normalize all date strings in the [Union Catalog of Digital Archives Taiwan](http://digitalarchives.tw/) to ISO8601 dates and generate a mapping table for original date strings and normalized dates.

## Input

The csv files converted from xml files of Union Catalog of Digital Archives Taiwan.

## Output

A csv file with the following fields:

| Field | Value |
| ----- | ----- |
| Date | original date string |
| normalized | normalized ISO8601 date |

# Dependencies

* Python 3.4

* Python dependencies

```
pip3 install -r requirements.txt
```

* Only tested on Ubuntu (>= 14.04)

```
sudo apt-get install build-essential python3-dev
```

# Usage

## Step.1: Normalize date strings and extract Chinese lunar calendar dates

* `python3 normalize.py -f CSV_FILE -o OUTPUT_FOLDER -e EXCLUDE_BEFORE_YEAR`
  * CSV\_FILE: The csv file converted from xml files.
  * OUTPUT\_FOLDER: The folder for output csv files. The csv file named `*_time_normalized.csv` is the result of normalization. The csv file named `*_dynasty.csv` contains the extracted Chinese lunar calendar dates.
  * EXCLUDE\_BEFORE\_YEAR: Exclude dates before a certain year.

## Step.2: Convert Chinese lunar calendar dates to ISO8601 dates using Chinese-Western Calendar Conversion Tool

* Upload the `*_dynasty.csv` generated from Step.1 to [Chinese-Western Calendar Conversion Tool](http://sinocal.sinica.edu.tw/) (In Chinese. Developed by Academia Sinica, Taiwan.)
* You may need to devide the csv file into several parts since the system only accept a csv with no more than 1,000 records.

## Step.3: Update the mapping table with the conversion result of Chinese-Western Calendar Conversion Tool

* `python3 update_dynasty.py -f CSV_FILE -d DYNASTY_CSV_FILE -o OUTPUT_FOLDER`
  * CSV\_FILE: The csv generated from Step.1.
  * DYNASTY\_CSV\_FILE: The csv generated from Step.2.
  * OUTPUT\_FOLDER: The folder for output csv files.