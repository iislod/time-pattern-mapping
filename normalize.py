import re
import datetime
from argparse import RawTextHelpFormatter, ArgumentParser

import rfGengou

import utils


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-f", metavar="CSV_FILE",
                    help="the csv file converted from xml files", required=True)
parser.add_argument("-o", metavar="OUTPUT_FOLDER",
                    help="the folder for output results", required=True)
parser.add_argument("-e", metavar="EXCLUDE_BEFORE_YEAR",
                    help="exclude dates before a certain year", required=False)

args = parser.parse_args()

patterns = [('%Y-%m-%d', '%Y-%m-%d'),
            ('%Y/%m/%d', '%Y-%m-%d'),
            ('%d/%m/%Y', '%Y-%m-%d'),
            ('%Y%m%d', '%Y-%m-%d'),
            ('%Y年%m月%d日', '%Y-%m-%d'),
            ('%Y/%m', '%Y-%m'),
            ('%Y/%m/', '%Y-%m'),
            ('%Y-%m', '%Y-%m'),
            ('%Y-%m-', '%Y-%m'),
            ('%Y-%m-00', '%Y-%m'),
            ('%Y年%m月', '%Y-%m'),
            ('%Y', '%Y'),
            ('%Y/', '%Y'),
            ('%Y--', '%Y'),
            ('%Y-00-00', '%Y'),
            ('西元%Y', '%Y'),
            ('西元%Y年', '%Y'),
            ('%Y年', '%Y'),
            ('%Y-%m-%d %H:%M:%S', '%Y-%m-%d'),
            ('%Y-%m-%d%H:%M:%S', '%Y-%m-%d'),
            ('%Y/%m/%d%H:%M:%S', '%Y-%m-%d'),
            ('資料修訂日期:%Y/%m/%d', '%Y-%m-%d'),
            ('%Y-%m---', '%Y-%m'),
            ('拍攝日期:%Y/%m/%d', '%Y-%m-%d'),
            ('拍攝日期:%d/%m/%Y', '%Y-%m-%d'),
            ('採集日期:%Y/%m/%d', '%Y-%m-%d'),
            ('採集日期:%d/%m/%Y', '%Y-%m-%d'),
            ('採集日期:%Y/%m%d', '%Y-%m-%d'),
            ('採集日期:%Y-%m-%d', '%Y-%m-%d'),
            ("採集日期:%d'%m'%Y", '%Y-%m-%d'),
            ('採集日期:%Y/%m', '%Y-%m'),
            ('採集日期:%Y/%m/', '%Y-%m'),
            ('採集日期:%Y-%m-', '%Y-%m'),
            ('採集日期:%Y', '%Y'),
            ('採集日期:%Y/', '%Y'),
            ('採集日期:%Y//', '%Y'),
            ('採集日期:%Y--', '%Y'),
            ('標本採集日期:%Y/%m/%d', '%Y-%m-%d'),
            ('標本採集日期:%Y/%m/', '%Y-%m'),
            ('標本採集日期:%Y/', '%Y'),
            ('標本登錄日期:%Y/%m/%d', '%Y-%m-%d'),
            ('標本採集時間:%Y/%m/%d', '%Y-%m-%d'),
            ('鑑定日期:%Y/%m/%d', '%Y-%m-%d'),
            ('鑑定時間:%Y/%m/%d', '%Y-%m-%d'),
            ('採集期間-起:%Y/%m/%d', '%Y-%m-%d'),
            ('採集期間-迄:%Y/%m/%d', '%Y-%m-%d'),
            ('發生時間:%Y/%m/%d', '%Y-%m-%d'),
            ('RecordingDate:%Y-%m-%d', '%Y-%m-%d'),
            ('RecordingDate:%Y-%m', '%Y-%m'),
            ('RecordingDate:%Y', '%Y'),
            ('製作時間:%Y', '%Y'),
            ('入藏時間:%Y年%m月%d日', '%Y-%m-%d'),
            ('入藏時間:%Y年%m月', '%Y-%m'),
            ('日期:%Y年%m月%d日', '%Y-%m-%d')]

not_matched_list = []

input_r = utils.load_csv(args.f, encoding='utf-8-sig')
dates = set(row['Date'] for row in input_r if row['Date'])
dates = list(dates)
dates.sort()
date_rows = [{'Date': date} for date in dates]

for row in date_rows:
    get_pattern = []

    # Remove noises
    remove = str.maketrans('', '', '[] ')
    date = row['Date'].translate(remove)
    date = date.replace('：', ':')
    date = date.replace('曰', '日')

    # For roman numbers
    date = date.replace('XII', '12')
    date = date.replace('ⅩⅡ', '12')
    date = date.replace('XI', '11')
    date = date.replace('ⅩⅠ', '11')
    date = date.replace('ⅩI', '11')
    date = date.replace('IX', '09')
    date = date.replace('Ⅸ', '09')
    date = date.replace('X', '10')
    date = date.replace('Ⅹ', '10')
    date = date.replace('VIII', '08')
    date = date.replace('Ⅷ', '08')
    date = date.replace('VII', '07')
    date = date.replace('Ⅶ', '07')
    date = date.replace('VI', '06')
    date = date.replace('Ⅵ', '06')
    date = date.replace('IV', '04')
    date = date.replace('Ⅳ', '04')
    date = date.replace('V', '05')
    date = date.replace('Ⅴ', '05')
    date = date.replace('III', '03')
    date = date.replace('Ⅲ', '03')
    date = date.replace('II', '02')
    date = date.replace('Ⅱ', '02')
    date = date.replace('I', '01')
    date = date.replace('Ⅰ', '01')

    for pattern in patterns:
        match = utils.match_pattern(date, pattern[0], pattern[1])
        if match:
            get_pattern.append(match)

    date = date.replace('日期:', '')

    if date[:2] == '民國' and not get_pattern:
        c_time = utils.parse_datetime(date[2:], roc=True)
        if c_time:
            c_time_normalized = c_time[
                'formatted_date'].strftime(c_time['format'])
            get_pattern.append({'formatted_date': c_time['formatted_date'],
                                'normalized_date': c_time_normalized})

    if date[:4] == '中華民國' and not get_pattern:
        c_time = utils.parse_datetime(date[4:], roc=True)
        if c_time:
            c_time_normalized = c_time[
                'formatted_date'].strftime(c_time['format'])
            get_pattern.append({'formatted_date': c_time['formatted_date'],
                                'normalized_date': c_time_normalized})

    if date[:2] in ['明治', '大正', '昭和', '平成'] and not get_pattern:
        j_time = utils.parse_datetime(date[2:])
        if j_time:
            j_time_converted = rfGengou.g2s(date[:2], j_time['formatted_date'].year, j_time[
                                            'formatted_date'].month, j_time['formatted_date'].day)
            if j_time_converted:
                j_time_normalized = j_time_converted.strftime(j_time['format'])
                get_pattern.append({'formatted_date': j_time_converted,
                                    'normalized_date': j_time_normalized})

    # Notify when matching more than two rules.
    if len(get_pattern) > 1:
        print('Matched more than two rules:', row['Date'])

    if date and get_pattern:
        if args.e:
            # Exclude date before a certain year.
            if get_pattern[0]['formatted_date'] < datetime.datetime(int(args.e), 1, 1):
                print('Date < %s: %s' %
                      (args.e, get_pattern[0]['normalized_date']))
                get_pattern[0]['normalized_date'] = ''
        row.update({'normalized': get_pattern[0]['normalized_date']})
    else:
        not_matched_list.append({'Date': row['Date']})

dynasty_rows = []
oid = ''
save_flag = False
regex = '(清)?(乾隆)?(嘉慶)?(道光)?(咸豐)?(同治)?(光緒)?(光緖)?([0-9]+)年([0-9潤閏]+)月([0-9]+)日'
for row in not_matched_list:
    date = row['Date'].replace('曰', '日')
    date = date.replace(' ', '')
    m = re.search(regex, date)
    if m:
        dynasty = list(filter(None, m.groups()[1:7]))
        if len(dynasty) == 1:
            row.update({'國號': m.group(1) or '清',
                        '年號': dynasty[0],
                        '年': m.group(9),
                        '月': m.group(10),
                        '日': m.group(11)})
            dynasty_rows.append(row)

# Output
filename = args.f.split('/')[-1][:-4] + '_time_normalized.csv'
utils.save_csv(['Date', 'normalized'], date_rows, args.o +
               '/' + filename, encoding='utf-8-sig')

dynasty_fields = ['Date', '國號', '帝號', '年號', '年', '月', '日']
filename = args.f.split('/')[-1][:-4] + '_dynasty.csv'
utils.save_csv(dynasty_fields, dynasty_rows, args.o +
               '/' + filename, encoding='utf-8-sig')
