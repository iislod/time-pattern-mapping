from argparse import RawTextHelpFormatter, ArgumentParser

import utils


parser = ArgumentParser(formatter_class=RawTextHelpFormatter)
parser.add_argument("-f", metavar="CSV_FILE",
                    help="the time pattern mapping csv file", required=True)
parser.add_argument("-d", metavar="DYNASTY_CSV_FILE",
                    help="the dynasty csv file", required=True)
parser.add_argument("-o", metavar="OUTPUT_FOLDER",
                    help="the folder for output results", required=True)
args = parser.parse_args()

input_r = utils.load_csv(args.f, encoding='utf-8-sig')
input_d = utils.load_csv(args.d, encoding='utf-8-sig')

dynasty_patterns = {row['Date']: row['normalized']
                    for row in input_d if row['normalized'][:2] != 'E:'}

out_rows = []
not_matched_list = []

for row in input_r:
    if dynasty_patterns.get(row['Date']):
        row['normalized'] = dynasty_patterns[row['Date']]
    out_rows.append(row)
    if not row['normalized']:
        not_matched_list.append({'Date': row['Date']})

filename = args.f.split('/')[-1][:-4] + '_after_dynasty.csv'
utils.save_csv(input_r.fieldnames, out_rows, args.o +
               '/' + filename, encoding='utf-8-sig')

if not_matched_list:
    not_matched_filename = args.f.split(
        '/')[-1][:-4] + '_not_matched_after_dynasty.csv'
    utils.save_csv(['Date'], not_matched_list, args.o + '/' +
                   not_matched_filename, encoding='utf-8-sig')
