"""Utility functions

Consists of functions shared by this program.
"""
import os
import re
import csv
import datetime


UTIL_CN_NUM = {
    '一': 1,
    '二': 2,
    '三': 3,
    '四': 4,
    '五': 5,
    '六': 6,
    '七': 7,
    '八': 8,
    '九': 9,
}
UTIL_CN_UNIT = {
    u'十': 10,
}


def load_csv(f, encoding="utf-8", fieldnames=None, prompt=True):
    """Load csv and get the DictReader object.
    """
    if prompt:
        print("Processing %s..." % f)
    csv_file = open(os.path.normpath(f), encoding=encoding)
    csv_reader = csv.DictReader(csv_file, fieldnames=fieldnames)

    return csv_reader


def save_csv(fieldnames, rows, f, encoding="utf-8"):
    """Save the DictWriter object to a csv file.
    """
    os.makedirs(os.path.dirname(os.path.abspath(f)), exist_ok=True)
    with open(os.path.normpath(f), "x", newline="", encoding=encoding) as fou:
        dw = csv.DictWriter(fou, fieldnames=fieldnames, dialect="unix")
        dw.writeheader()
        dw.writerows(rows)


def match_pattern(date_string, pattern, normalized):
    """Try to map date strings to known patterns.
    """
    try:
        formatted_date = datetime.datetime.strptime(date_string, pattern)
        normalized_date = formatted_date.strftime(normalized)
        return {'formatted_date': formatted_date, 'normalized_date': normalized_date}
    except ValueError:
        return


def cn2dig(src):
    """Convert Chinese numerals to Arabic numerals.
    """
    if src == "":
        return None
    m = re.match("\d+", src)
    if m:
        return m.group(0)
    rsl = 0
    unit = 1
    for item in src[::-1]:
        if item in UTIL_CN_UNIT.keys():
            unit = UTIL_CN_UNIT[item]
        elif item in UTIL_CN_NUM.keys():
            num = UTIL_CN_NUM[item]
            rsl += num * unit
        else:
            return None
    if rsl < unit:
        rsl += unit
    return str(rsl)


def parse_datetime(msg, roc=False):
    """Parse a Chinese date string to a group containing year, month, and day.
    """
    if msg is None or len(msg) == 0:
        return None
    m = re.search(
        r"([0-9一二三四五六七八九十]+年)?([0-9一二三四五六七八九十]+月)?([0-9一二三四五六七八九>十]+[日])?", msg)
    if m.group(0) is not None:
        res = {
            "year": m.group(1),
            "month": m.group(2),
            "day": m.group(3)
        }
        params = {}
        try:
            for name in res:
                if res[name] is not None and len(res[name]) != 0:
                    params[name] = int(cn2dig(res[name][:-1]))
        except Exception as e:
            return None
        if roc and params.get('year'):
            params['year'] += 1911
        try:
            formatted_date = datetime.datetime.today().replace(day=1).replace(**params)
            params_keys = params.keys()
            normalized_format = ''
            if 'year' in params_keys and 'month' in params_keys and 'day' in params_keys:
                normalized_format = '%Y-%m-%d'
            elif 'year' in params_keys and 'month' in params_keys and 'day' not in params_keys:
                normalized_format = '%Y-%m'
            elif 'year' in params_keys and 'month' not in params_keys and 'day' not in params_keys:
                normalized_format = '%Y'
            if normalized_format:
                return {'formatted_date': formatted_date, 'format': normalized_format}
            return None
        except Exception as e:
            return None
    else:
        return None
